1.
    a.$ java -XX:+PrintGCDetails GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        PSYoungGen      total 75776K, used 2600K [0x000000076b980000, 0x0000000770e00000, 0x00000007c0000000)
        eden space 65024K, 4% used [0x000000076b980000,0x000000076bc0a3f8,0x000000076f900000)
        from space 10752K, 0% used [0x0000000770380000,0x0000000770380000,0x0000000770e00000)
        to   space 10752K, 0% used [0x000000076f900000,0x000000076f900000,0x0000000770380000)
        ParOldGen       total 173568K, used 0K [0x00000006c2c00000, 0x00000006cd580000, 0x000000076b980000)
        object space 173568K, 0% used [0x00000006c2c00000,0x00000006c2c00000,0x00000006cd580000)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
    b.$ java -Xmx128m -XX:+PrintGCDetails GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        PSYoungGen      total 38400K, used 1996K [0x00000000fd580000, 0x0000000100000000, 0x0000000100000000)
        eden space 33280K, 6% used [0x00000000fd580000,0x00000000fd773348,0x00000000ff600000)
        from space 5120K, 0% used [0x00000000ffb00000,0x00000000ffb00000,0x0000000100000000)
        to   space 5120K, 0% used [0x00000000ff600000,0x00000000ff600000,0x00000000ffb00000)
        ParOldGen       total 87552K, used 0K [0x00000000f8000000, 0x00000000fd580000, 0x00000000fd580000)
        object space 87552K, 0% used [0x00000000f8000000,0x00000000f8000000,0x00000000fd580000)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
    c. $ java -Xmx512m -Xms512m  -XX:+UseSerialGC -XX:+PrintGCDetails GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        def new generation   total 157248K, used 5591K [0x00000000e0000000, 0x00000000eaaa0000, 0x00000000eaaa0000)
        eden space 139776K,   4% used [0x00000000e0000000, 0x00000000e0575c48, 0x00000000e8880000)
        from space 17472K,   0% used [0x00000000e8880000, 0x00000000e8880000, 0x00000000e9990000)
        to   space 17472K,   0% used [0x00000000e9990000, 0x00000000e9990000, 0x00000000eaaa0000)
        tenured generation   total 349568K, used 0K [0x00000000eaaa0000, 0x0000000100000000, 0x0000000100000000)
        the space 349568K,   0% used [0x00000000eaaa0000, 0x00000000eaaa0000, 0x00000000eaaa0200, 0x0000000100000000)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
    d.  $ java -Xmx512m -Xms512m  -XX:+UseParallelGC -XX:+PrintGCDetails GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        PSYoungGen      total 153088K, used 5263K [0x00000000f5580000, 0x0000000100000000, 0x0000000100000000)
        eden space 131584K, 4% used [0x00000000f5580000,0x00000000f5aa3d98,0x00000000fd600000)
        from space 21504K, 0% used [0x00000000feb00000,0x00000000feb00000,0x0000000100000000)
        to   space 21504K, 0% used [0x00000000fd600000,0x00000000fd600000,0x00000000feb00000)
        ParOldGen       total 349696K, used 0K [0x00000000e0000000, 0x00000000f5580000, 0x00000000f5580000)
        object space 349696K, 0% used [0x00000000e0000000,0x00000000e0000000,0x00000000f5580000)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
    e.  $ java -Xmx512m -Xms512m  -XX:+UseConcMarkSweepGC -XX:+PrintGCDetails GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        par new generation   total 157248K, used 5591K [0x00000000e0000000, 0x00000000eaaa0000, 0x00000000eaaa0000)
        eden space 139776K,   4% used [0x00000000e0000000, 0x00000000e0575c48, 0x00000000e8880000)
        from space 17472K,   0% used [0x00000000e8880000, 0x00000000e8880000, 0x00000000e9990000)
        to   space 17472K,   0% used [0x00000000e9990000, 0x00000000e9990000, 0x00000000eaaa0000)
        concurrent mark-sweep generation total 349568K, used 0K [0x00000000eaaa0000, 0x0000000100000000, 0x0000000100000000)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
    f.$ java -XX:+UseG1GC -Xms512m -Xmx512m -XX:+PrintGCDetails -XX:+PrintGCDateStamps GCLogAnalysis
        ▒▒▒▒: ▒Ҳ▒▒▒▒▒▒޷▒▒▒▒▒▒▒▒▒ GCLogAnalysis
        Heap
        garbage-first heap   total 524288K, used 1024K [0x00000000e0000000, 0x00000000e0101000, 0x0000000100000000)
        region size 1024K, 2 young (2048K), 0 survivors (0K)
        Metaspace       used 2790K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 316K, capacity 384K, committed 384K, reserved 1048576K
2.null
3.null
4.不同 GC 和堆内存的总结
    a.串行GC jdk默认gc方式 如果没有配置-XX:+Use 默认使用这个 单线程 慢
    b.并行GC  多线程gc 比串行化gc快
    c.CMSGC   
    d.G1GC   gc参数比较多 需要合理配置 不然会退化成串行gc 产生大量gullgc

5.
6.[](https://gitlab.com/buwaia/geek/-/blob/master/data/HttpServer01.java)
